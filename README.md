# Light Cycles Network Protocol

Wir bauen ein Netzwerkprotokoll auf REST-Basis für das Spiel Light Cycles. Das Protokoll soll es ermöglichen, dass mehrere Clients miteinander spielen können. Die Kommunikation zwischen den Clients soll über ein lokales Netzwerk stattfinden.  

## Anforderungen

Das Protokoll wird nicht verwendet, um ein Spiel komplett als Server-Client-Anwendung laufen zu lassen wo die gesamte Logik an einem Server läuft. Das Protokoll soll lediglich die Eingaben der Clients an alle anderen Supernode weitergeben, sodass diese dort verarbeitet werden können. Jeder Supernode übernimmt die Verantwortung die Logik für seine Clients und sich selbst zu deligieren oder auszuführen. Somit werden die Spiele in ihren Zuständen synchronisiert.

Es wird davon ausgegangen, dass ein Koordinator gewählt wird bevor die hier definierten Nachrichten verschickt werden, und dass die jeweilige Implementierung selbst bestimmt, wie der Koordinator gefunden wird (z. B. hardcoded oder über einen Nameservice). 

Festgelegt ist, dass die Ressource "tron.coordinator" heißt wie folgt hinterlegt wird:

**HTTP-Methode**: `PUT`  
**Route**: `/entry`  
Beispiel-Payload:  

```json
{
    "name": "tron.coordinator",
    "address":"http://ip:port"
}
``` 

Auch ist selbst zu entscheiden, wie mit einer zu geringen Anzahl an Spielern umgegangen wird (Timer im Koordinator oder weiter warten)

## Randbedinungen

Wenn es sich bei einer URI, um eine IPv6 Adresse handelt, muss diese nach RFC 2732 entsprechend in eckige Klammern eingefasst werden. z.B. `http://[::1]:8080`

## Charakterisierung

- Application-Layer
- nicht verbindungsorientiert
- HTTP

## Schlüsselbegriffe

- "Client": Ein Client ist ein am Spiel beteiligter Prozess der ein LightCycle steuert. 
- Zustände des Spiels: In welche Richtung jedes LightCycle im jeweils nächsten Tick abbiegen möchte und ob das Spiel losgeht.
- "Koordinator": Ein Prozess der ein neues Spiel organisiert. Hier können sich Client zum Spielen registrieren. Er bestimmt, wann das Spiel startet und die Startpositionen und teilt den Clients die Adressen der anderen Clients mit.
- "Supernode": Ein Prozess, der ein Gateway im REST-Netzwerk darstellt. Er ist dafür zuständig, die Nachrichten der per RPC an ihn angeschlossenen Clients per REST-Protokoll (definiert in diesem Dokument) an die anderen Supernodes weiterzuleiten und Nachrichten anderer Supernodes zu verarbeiten, um die Spiele miteinander zu synchronisieren.

## Ressourcen

- **REGISTRATION**: Teilt dem Koordinator mit, dass man spielen möchte, und enthält die zu registrierende Spieleranzahl. Erwartet eine Antwort, die mitteilt, ob die Registrierung angenommen oder abgelehnt wurde.
- **GAME**: Wenn der Koordinator feststellt, dass die gewünschte Spieleranzahl erreicht ist, wird das Spiel gestartet. Als Nutzdaten werden die Adressen der Supernodes, die IDs der dort angeschlossenen Spieler, die Startpositionen und Startrichtungen aller Spieler übertragen. Diese Nachricht ist auch das Signal zum Starten des Countdowns. 
- **STEERING**: Wird während des Spiels von einem Supernode zu allen anderen Supernodes gesendet, wenn dort ein Spieler steuern möchte. Enthält die Spieler-ID und die Richtung, in die gelenkt werden soll. 

## Ablaufsemantik

### PUT REGISTRATION

#### Vorbedingungen
- Es existiert ein Koordinator.
- Supernode kennt die Adresse des Koordinators.
#### Nachbedingungen
- Supernode ist beim Koordinator registriert oder selbst Koordinator
#### Ablauf
1. Supernode sendet REGISTER an den Koordinator
2. Der Koordinator prüft, ob noch Platz vorhanden ist
3. Der Koordinator nimmt die erhaltenen Clients mit der ID/Adresse des Supernodes in eine Liste auf
4. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `200`
5. Supernode bekommt den HTTP-Code `200` zurück
#### Erweiterungsfall 3a) Es ist kein Platz mehr vorhanden
1. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `406` 
2. Supernode bekommt den HTTP-Code `406` zurück
3. Der Supernode wird nicht registriert und startet sein Spiel lokal
#### Erweiterungsfall 3b) Das Spiel ist schon voll bzw. es läuft schon
1. Der Koordinator antwortet dem Supernode mit dem HTTP-Code `410`
2. Supernode bekommt den HTTP-Code `410` zurück
3. Der Supernode trägt sich selbst als Koordinator beim NameServer ein
#### Erweiterungsfall 3c) Der Supernode bekommt keine Antwort
1. Der Supernode hat nach einer Sekunde noch keine Antwort vom Koordinator bekommen
2. Der Supernode trägt sich selbst als Koordinator beim NameServer ein


### PUT GAME

#### Vorbedingungen
- Nach Verarbeitung eines REGISTERs sind genügend Clients zum Starten eines Spiels vorhanden
- Alle Light Cycles haben definierte Startpositionen, -richtungen und IDs
#### Nachbedingungen
- Alle Supernodes kennen die Startpositionen, Startrichtungen, IDs aller Clients und Adressen aller Supernodes
- Bei allen Clients läuft der Countdown
- Koordinator ignoriert weitere REGISTERs
#### Ablauf
1. Der Koordinator sendet die Startpositionen, -richtungen und IDs an die registrierten Supernodes
2. Die Supernodes setzen die gesendeten Daten in ihrem Spiel
3. Alle Supernodes starten den Countdown

### PUT STEERING

#### Vorbedingungen
- Das Spiel ist gestartet
#### Nachbedingungen
- Die Richtung des jeweiligen Light Cycles ist dem Inhalt der STEERING-Nachricht entsprechend gesetzt
#### Ablauf
1. Supernode sendet STEERING an alle bekannten Supernodes
2. Alle Supernodes setzen die Richtung des betreffenden Light Cycles neu

## Fehlersemantik

Wenn eine eingehene Nachricht nicht den hier beschriebenen Formaten entspricht, wird sie ignoriert. 

## Kodierung

Alle Payloads werden als JSON kodiert mitgesendet.
Die entsprechenden Objekte sind wie folgt aufgebaut.

### REGISTRATION

**HTTP-Methode**: `PUT`  
**Route**: `/registration`  
Beispiel-Payload:  

```json
{
    "playerCount": 3,
    "uri": "http://192.123.123.123/24"
}
``` 

### GAME

**HTTP-Methode**: `PUT`  
**Route**: `/game`  
**Anmerkung**: Die LightCycles innerhalb eines Supernodes, besitzen nur direkt aufeinander folgende IDs, welche in aufsteigender Reihenfolge sortiert sind *(bsp. 0,1,2 und nicht 2,0,1)*. Über die enthaltenen IDs werden auch die Supernodes in der Liste aufsteigend sortiert. *(bsp. [{Supernode mit 0,1,2}, {Supernode mit 3,4,5}])*. Die `direction` wird angegeben als einer der Werte `{"UP", "RIGHT", "DOWN", "LEFT"}`
Beispiel-Payload:

```json
[
    { // Supernode 1
        "address": "http://localhost:8080",
        "lightCycles": [
            { // LightCycle
                "id": 0,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "RIGHT"
            },
            {
                "id": 1,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "UP"
            }
        ]
    },
    { // Supernode 2
        "address": "http://[fd3e:fa5c:1234:5678:9abc:defg:hijk:lmno]:8081",
        "lightCycles": [
            {
                "id": 2,
                "position": {
                    "x": 0,
                    "y": 0
                },
                "direction": "RIGHT"
            }
        ]
    }
]
```

### STEERING

**HTTP-Methode**: `PUT`  
**Route**: `/steering`  
Beispiel-Payload:

```json

{
    "id": 1,
    "turn": "RIGHT"
}
```